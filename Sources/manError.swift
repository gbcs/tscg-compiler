
class Error{
  //var tipo:Error.Tipo
  var descripcion:String
  var nlinea:Int
  var nchar:Int
  init(_ descripcion:String,_ nlinea:Int,_ nchar:Int){
    self.descripcion=descripcion
    self.nlinea=nlinea
    self.nchar=nchar
  }
}
class ManError{
  static var errores=[Error]()
  static func newError(descripcion:String,_ nlinea:Int=0,_ nchar:Int=0){
    errores.append(Error(descripcion,nlinea,nchar))
  }


  static func imprimir(){
    for error in errores{
      print("Error: \(error.descripcion) linea: \(error.nlinea)")
    }
  }
}
