
struct Token {
    var nombre:String
    var atributo:String
    var valor:String
    var nlinea:Int
    init(_ nombre:String="",_ atributo:String="",_ valor:String="",_ nlinea:Int = -1){
      self.nombre=nombre
      self.atributo=atributo
      self.valor=valor
      self.nlinea=nlinea
    }
}
