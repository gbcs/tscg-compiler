
struct P{
    let palabra:String
    let opcional:Bool
    let grupo:Bool
    var conop:Bool
    let tipo:String

    var opciones=[P]()
    init(_ palabra:String, _ tipo:String,_ opcional:Bool=false,_ grupo:Bool=false,_ opciones:[P]=[]){
        self.palabra=palabra
        self.opcional=opcional
        self.grupo=grupo
        self.tipo=tipo
        self.conop=false
        self.opciones=opciones
        if(opciones.count>0){
            self.conop=true
        }
    }

}

class Gram{
    let nombre:String
    let produccion:String
    var Ps:[P]=[]


    init(_ nombre:String,_ produccion:String){
        self.nombre=nombre
        self.produccion=produccion

        //genPs()


    }

    func genPs(grupo:Bool=false){

        var array = produccion.characters.split{$0 == (grupo ? ",":" ")}.map(String.init)



      for var x in array{
        var op=false
        var gr=false
        var conop=false
        var opciones=[P]()
        if(x.characters[x.endIndex.advancedBy(-1)]=="?"){
          op=true
          x.removeAtIndex(x.endIndex.advancedBy(-1))
        }//quitamos ? y marcamos como opcional
        if(x.characters[x.startIndex]=="(" && x.characters[x.endIndex.advancedBy(-1)]==")"){
          x.removeAtIndex(x.endIndex.advancedBy(-1))
          x.removeAtIndex(x.startIndex)//quitamos parentesis

          let nnom="grupo"+String(Gram.gindex)
          Gram.gindex+=1

          var tmp=Gram(nnom,x)
          x=nnom
          Gram.grams.append(tmp)
          tmp.genPs(true)

        }else{//grupo simple

          var arrayOp = x.characters.split{$0 == "|"}.map(String.init)
          if(arrayOp.count>1){
            conop=true
            for o in arrayOp{

              opciones.append(P(o,ckTipo(o)))

            }//opciones creadas
          }


        }

        Ps.append(P(x,(conop ? "grOp" : ckTipo(x)),op,gr,opciones))
      }
    }
    func ckTipo(pal:String)->String{

        for gram in Gram.grams{
            if(pal==gram.nombre){return "gramatica"}
        }
        for exp in Expr.expr{
            if(pal==exp.nombre){return "token"}
        }
        for exp in Expr.pres{
            if(pal==exp.nombre){return "token"}
        }

        return "desconocido"
    }




    static func isGram(pal:String)->Int{
        var i=0;
        for g in Gram.grams{
            i+=1
          //  print("revisando Gram ",pal,"==",g.nombre,"?")
            if(pal==g.nombre){return i-1}

        }
        return -1
    }
    static var gindex=0
    static var grams=[
  //Gram("prueba","gets importar? inicio (importar,gets)?"),
  Gram("archivo","defImportaciones? defroom? defclases? deffunciones? mainBloque"),
    Gram("defImportaciones","gets inicio salto importaciones? terminacion"),
      Gram("importaciones","importacion importaciones?"),
        Gram("importacion","importar (string|identificador) identificador? salto"),
    Gram("defroom","room identificador salto"),
    Gram("defclases","modules inicio salto clases? terminacion"),
    Gram("clases","clase clases?"),
      Gram("clase","modificadores? modulo identificador (parIni,identificador,parFin)? inicio salto instrucciones? constructor? instrucciones? destructor? instrucciones? terminacion"),
        Gram("constructor","make bloqueDec bloque"),
        Gram("destructor","umake bloque"),
    Gram("deffunciones","fns inicio salto funciones? terminacion"),
      Gram("funciones","funcion funciones?"),
        Gram("funcion","modificadores? decFn identificador bloqueDec  tipoRetorno tipoDato  bloque"),
  Gram("bloqueDec","parIni declaraciones? parFin"),
  Gram("mainBloque", "main  bloque"),
  Gram("sbloque", "inicio salto instrucciones? salto"),///provicional
  Gram("bloque", "inicio salto instrucciones? terminacion  "),

   Gram("prueba","inicio terminacion? salto"),
   Gram("prueba2"," prueba? inicio"),
   Gram("prueba3","terminacion (salto|terminacion)"),


   Gram("modificadores","modificador modificadores?"),
   Gram("declaraciones","declaracion declaraciones?"),
   Gram("declaracion","modificadores? definicion identificador inicio tipoDato opAsignacion?"),

   Gram("operacion","valor oprMat valor"),

   //Gram("operacionUni","((id,opeUni)|(opeUni,id))"),

   Gram("valor","(identificador|llamadoFn|literal|grpOp)"),
   Gram("grpOp","parIni operaciones parFin"),
   Gram("literal","(string|numero|boolVal)"),
   Gram("aux1","identificador"),
   Gram("asignar","identificador opAsignacion"),
   Gram("operacionUni","identificador opUnit"),
   Gram("opAsignacion","(asignacion|op+igual) (valor|operacines)"),
    Gram("instrucciones","instruccion instrucciones?"),
  //  Gram("sinstruccion","()"),//quite estructura
  Gram("instruccionSimple","operacionUni|asignar"),//quite estructura
  Gram("instruccion","(declaracion|asignar|estructura|llamadoFn|estRetorno|interrupcion|defEvento|emitir|conectar|defArray|defList) salto"),
  Gram("estructura","estPregunta|estLoop|estTloop|estCloop|estNor|estRouter"),//para prueba
  Gram("declaracion","modificadores? definicion identificador inicio tipoDato opAsignacion?"),//para prueba

  Gram("llamadoFn","parIni parametros? parFin salto"),
  Gram("estRetorno","retorno (valor|operacines)?"),
  Gram("operaciones","(operacionUni|operacion) (oprMat,operaciones)?"),

  Gram("parametros","identificador"),//falta

  Gram("estPregunta","pregunta condiciones bloque grpEstElif? estNif? "),
Gram("grpEstElif","estElif grpEstElif?"),
  Gram("estElif","elif condiciones bloque"),//falta else if extras
  Gram("estNif","nif bloque"),
    Gram("condiciones","condicion (oprLog,condiciones)?"),
    Gram("condicion","valor (oprComp,valor)?"),
    Gram("estLoop","loop identificador in iterable bloque"),
    Gram("iterable","identificador|estRango"),
    Gram("estRango","valor rango valor"),
    Gram("estTloop","tloop declaraciones?  terminacionSimple condiciones? terminacionSimple instruccionSimple? bloque"),
    Gram("estCloop","cloop condiciones tipoRetorno? bloque"),
    Gram("estRouter","router valor inicio salto ports? estNotport salto? terminacion"),
    Gram("ports","estPort instrucciones? ports?"),
    Gram("estPort","port ll bloque"),
    Gram("ll","literal (separacion,ll)?"),
    Gram("estNotport","notPort bloque"),
    Gram("estNor","capError bloque ports estRest terminacion"),
    Gram("estRest","rest bloque"),
    Gram("defLista","list definicion identificador inicio tipoDato asigLL?"),
    Gram("asigLL","asignacion literalList"),
    Gram("literalList","corchIni ll corchFin"),
    Gram("defEvento","signal identificador literalList "),
    Gram("emitir","emit identificador"),
    Gram("conectar","identificador link llamadoFn"),
    Gram("defArray","array  identificador inicio tipoDato (asigLL|defTam)"),
    Gram("defList","list identificador inicio tipoDato asigLL?"),
    Gram("defTam","corchIni numero corchFin"),
//, i=i+1

    ]
}
