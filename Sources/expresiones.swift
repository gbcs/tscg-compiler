
struct Expresion{
  let nombre:String
  let regex:String
  init(_ nombre:String,_ regex:String){
    self.nombre=nombre
    self.regex=regex
  }
}

class Expr{
  static let expr = [
    Expresion("salto","^\\n"),

    //Expresion("agrupacion","^[\\[\\]\\{\\}\\[\\]\\(\\)]"),
    Expresion("corchIni","^\\["),
    Expresion("corchFin","^\\]"),
    Expresion("llaveIni","^\\{"),
    Expresion("llaveFin","^\\}"),
    Expresion("parIni","^\\("),
    Expresion("parFin","^\\)"),

    //Expresion("string","^\"[^\"]*\"|^\'[^\']*\'"),
    Expresion("string","^\"[^\"\\\\\\r\\n]*(?:\\\\.[^\"\\\\\\r\\n]*)*\"|^\'[^\'\\\\\\r\\n]*(?:\\\\.[^\'\\\\\\r\\n]*)*\'"),
    Expresion("comentario","^#.*\\n"),
    Expresion("comentariob","^@>[^(<@)]*<@"),
  //  Expresion("comentarioc","^@>[^(<@)]*<@"),
    Expresion("tipoRetorno","^->"),
    Expresion("retorno","^<<"),
    Expresion("elif","^\\?:|^:if"),
      Expresion("nif","^!:|^nif"),
    Expresion("op+igual","^\\+=|^-=|^\\*=|^\\*\\*=|^iz="),
    Expresion("opUnit","^\\+\\+|^--"),
    Expresion("oprMat","^//|^\\*\\*"),
    Expresion("oprMat","^-|^\\+|^/|^%|^\\*|^\\^"),
    Expresion("oprComp","^<=|^>=|^!=|^==|^<\\?|^!<\\?|^\\?>|^!\\?>"),
    Expresion("oprComp","^<|^>"),
    Expresion("oprLog","^&&|^\\|\\||^!"),
    Expresion("asignacion","^="),
    Expresion("pregunta","^\\?:"),//50
    Expresion("rango","^\\.\\.\\."),
    Expresion("puntuacion","^\\."),
    Expresion("terminacion","^;;"),
    Expresion("terminacionSimple","^;"),
    Expresion("separacion","^,"),
    Expresion("inicio","^:"),

    Expresion("self","^\\$"),
    Expresion("numero","^((\\d+)?\\.\\d+)[eE][+-]?\\d+\\W"),
    Expresion("numero","^((\\d+)?\\.\\d+)([eE][+-]?\\d+\\W)?"),
    Expresion("numero","^\\d+\\W"),
    Expresion("espacio","^ |^\\t"),
    Expresion("crear","^~"),

    Expresion("identificador","^[a-zA-Z_]\\w*"),//150
    Expresion("otro","[\\s;,.!#%^*(){}\\[\\]+=<>\\-]|@>"),
    //Expresion("fin","$"),
  ]
  static let pres = [
  Expresion("in","^in$"),
  Expresion("router","^router$"),
  Expresion("preguntaMult","^router$"),
  Expresion("loop","^loop$"),
  Expresion("tloop","^tloop$"),
  Expresion("cloop","^cloop$"),
  Expresion("interrupcion","^next$|^stop$"),
  Expresion("port","^port$"),
  Expresion("notPort","^notport$"),
  Expresion("definicion","^def$|^var$"),
  Expresion("modulo","^mod$"),
  Expresion("modificador","^sinsig$|^free$|^lock$|^mable$|^ever$|^ext$|^shield$"),
  Expresion("signal","^signal$"),
  Expresion("list","^list$"),
  Expresion("array","^array$"),
  Expresion("emit","^emit$"),
  Expresion("link","^link$"),
  Expresion("dentro","^in$"),
  Expresion("data","^data$"),
  Expresion("padre","^parent$"),
  Expresion("decFn","^fn$"),
  Expresion("tipoDato","^ul$|^num$|^text$|^decimal$|^real$|^bool$|^enum$|^vec2d$|^vec3d$|^vec4d$|^group$|^bites$|^list$|^cad$|^dic$|^item$|^snum$|^sdecimal$|^sreal$"),
  Expresion("boolVal","^true$|^false$|^yes$|^no$"),//101
  Expresion("room","^room$"),
  Expresion("null","^nul$"),
  Expresion("pregunta","^if$"),
  Expresion("tul","^tul$"),
  Expresion("capError","^nor$"),
  Expresion("rest","^rest$"),
  Expresion("tipovar","^itype$"),
  Expresion("cast","^to$"),
  Expresion("case","^case$"),
  Expresion("constante","^let$"),
  Expresion("aleatorio","^rand$"),
  Expresion("default","^default$"),
  Expresion("tamaño","^isize$"),
  Expresion("tris","^gsen$|^gcos$|^gtan$|^gcot$|^gsec$|^gcsec$|^gvers$|^gcover$|^rsen$|^rcos$|^rtan$|^rcot$|^rsec$|^rcsec$|^rvers$|^rcover$"),
  Expresion("infinito","^sinfin$"),
  Expresion("pi","^pi$"),//130
  Expresion("euler","^meu$"),
  Expresion("logaritmo","^ln$"),
  Expresion("make","^make$"),
  Expresion("umake","^umake$"),

  Expresion("potencia","^wer$"),
  Expresion("raiz","^iz$"),
  Expresion("salida","^out$|^outln$|^print$|^println$"),
  Expresion("entrada","^inp$"),
  Expresion("crear","^new$"),
  Expresion("tiempo","^wait$"),
  Expresion("hora","^time$"),
  Expresion("fecha","^today$"),
  Expresion("borrar","^del$"),
  Expresion("importar","^get$|^import$"),
  Expresion("gets","^gets$"),
  Expresion("modules","^modules$"),
  Expresion("fns","^fns$"),
  Expresion("main","^main$"),

  ]

}
