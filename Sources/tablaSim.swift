


class TablaSim {

  var tablaSim = [String]()
  
  func existInTable(id:String)->Bool{
    for item in tablaSim{
      if(item == id){ return true}
    }
    return false
  }
  func indexOf(id:String)-> Int{
    var i=0
    for item in tablaSim{
      if(item == id){ return i}
      i+=1
    }
    return -1
  }
  func addId(id:String)->Int{
    if(!existInTable(id)){
      tablaSim.append(id)
      return tablaSim.count-1
    }
    return indexOf(id)
  }

}
