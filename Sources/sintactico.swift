import Foundation

class Pila{
    static var pila=[Token]()
    static var pila2=[Token]()
    static func remover(ini:Int,_ fin:Int = -1){
        let fin=(fin>0&&fin<pila.count ? fin:pila.count)
        for i in ini..<fin{
            pila.removeAtIndex(i)
        }
    }
    static func sacar(ini:Int){
        while ini==pila.count{
            let pop=pila.popLast()!
          //  print(pop.nombre,"sacado de",pila.count)
            pila2.append(pop)
            //print(pop.nombre,"guardado pila2 en",pila2.count)

        }

    }
   static func append(s:Token)->Int{
    pila.append(s)
  //  print(s.nombre,"insertado en ",pila.count)
    return pila.count}
}

class Sintactico{

  var lex:Lexico

  init(url:String){
    lex = Lexico(url:url)
    for g in Gram.grams{
      g.genPs();
    }
    verificar()
   //printList()
   exec()

  }
  func printList(){
    print("token                | Atributo             |")
    for _ in 0...66{print("_",terminator:"")}
    print()
    var item=0
      while let token=lex.nextToken(){
        print("[\(token.nlinea)]",terminator:"")
        print(token.nombre,terminator:"")
        for _ in 0...(21-token.nombre.characters.count){print(" ",terminator:"")}
        print("|",token.atributo,terminator:"")
        for _ in 0...(21-token.atributo.characters.count){print(" ",terminator:"")}
        print("|",terminator:"")
        print(token.valor,terminator:"")
        print()

        item+=1
      }

      ManError.imprimir()
  }
  func exec(){


var b=false

   if(ck(Gram.grams[0],0,&b)){
       print("Archivo validado")
   }else{
       print("Errores")

   }
     ManError.imprimir()
  }




  var tokenEsperado=""
  var ultioToken=Token()
  func ck(gram:Gram,_ index:Int,inout _ bext:Bool)->Bool{
    var bint=false
    if(gram.Ps.count==index){
      bext=false
      print("gram: ",gram.nombre)
      return true;
    }
    if let token:Token = Pila.pila2.popLast() ?? lex.nextToken(){
      let pal=gram.Ps[index]

      if(token.nombre == "salto" && index==0){
            print("saltando espacio")

            return ck(gram,index,&bext)
      }
      let ipila=Pila.append(token)
      print("siguiente:",token.nombre)
      if(pal.tipo=="token"){
        print("revisando token",pal.palabra)
        if(pal.palabra==token.nombre){
          print("es",token.nombre)
          ultioToken=token

          bext=true
          return ck(gram,index+1,&bint)
        }else if(pal.opcional){
          //print("saltando token opcional",pal.palabra)

          Pila.sacar(ipila)
          return ck(gram,index+1,&bint)
        }else{
          print("NO es",pal.palabra)
          tokenEsperado=pal.palabra
          Pila.sacar(ipila)
          return false
        }
      }else if(pal.tipo=="gramatica"){
      //  print("revisando gramatica",pal.palabra)
        Pila.sacar(ipila)
         let i = Gram.isGram(pal.palabra)
         let res = ck(Gram.grams[i],0,&bint)
         if(res){
           bext=bint;
           return ck(gram,index+1,&bint)}

           else if(bint){
             bext=bint;
             //print("Error se esperaba:",tokenEsperado,"despues de",(ultioToken.nombre ?? "|"), (ultioToken.nlinea ?? "|"),":",token.nlinea)
             if(Gram.grams[i].Ps.count>index){
             print("Error se esperaba:",Gram.grams[i].Ps[index].palabra ?? "","despues de",(Pila.pila[Pila.pila.count-1].nombre ?? "|"), (Pila.pila[Pila.pila.count-1].nlinea ?? "|"),":",token.nlinea)
          }
             return false
           }else if(pal.opcional ){
             print("saltando gramatica opcional",pal.palabra)
             Pila.sacar(ipila)
             return ck(gram,index+1,&bint)
           }else{
            print("Error en ",pal.palabra)
           }
         return res


      }else if(pal.tipo=="grOp"){
        //print("revisando grOp",pal.palabra)
        Pila.sacar(ipila)
        let res=ckGr(pal.opciones,token,&bint)
        if(res){
          bext=bint;
          return ck(gram,index+1,&bint)}

          else if(bint){
            bext=bint;
            print("Error se esperaba:",tokenEsperado,"despues de",(ultioToken.nombre ?? "|"), (ultioToken.nlinea ?? "|"),":",token.nlinea)
            //print("Error se esperaba:",Gram.grams[i].Ps[index+1] ?? "","despues de",(Pila.pila[Pila.pila.count-1].nombre ?? "|"), (Pila.pila[Pila.pila.count-1].nlinea ?? "|"),":",token.nlinea)

            return false
          }else if(pal.opcional){
            Pila.sacar(ipila)
            return ck(gram,index+1,&bint)
          }
          return res

      }
      print("desconocido////////////////")
       readLine() ?? ""
    }


    print(index,gram.Ps[index].palabra)
    return false
  }






  func ckGr(ops:[P],_ palabra:Token,inout _ bext:Bool)->Bool{
    var res=false
    for op in ops{
      let tmp=Gram("tmp",op.palabra)
      tmp.genPs()
      let ret = ck(tmp,0,&bext)
      if(ret){return true}

    }
    return res
  }

  func verificar(){

    print("verificando gramaticas")
    for g in Gram.grams{
      print("\(g.nombre) : ")
      for p in g.Ps{

          print("\t\(p.palabra):\(p.tipo):\(p.opcional)")
          if(p.conop){
            for o in p.opciones{
              print("\t\t\(o.palabra):\(o.tipo):\(o.opcional)")
            }
          }


      }
    }


  }


}
