import Foundation
class Lexico{
  var fileContent:String
  //var tokenArray=[Token]()
  var rangeP:Range<String.Index>
  let tablaSim=TablaSim()
  var nlinea=1
  init(url:String){
    let location = NSString(string: url).stringByExpandingTildeInPath
    fileContent = String((try? NSString(contentsOfFile: location , encoding: NSUTF8StringEncoding))!)
    rangeP=fileContent.startIndex..<fileContent.endIndex
  }
  func nextToken()->Token?{
    if rangeP.startIndex < fileContent.endIndex{
      for expr in Expr.expr{
        if let result = fileContent.regex(expr.regex,range:rangeP){
          switch expr.nombre {

            case "espacio","comentario","comentariob":
              let sub = fileContent.substringWithRange(rangeP.startIndex..<result.endIndex)
              for char in sub.characters{if(char=="\n"){nlinea+=1}}
              //if(expr.nombre=="comentario"){nlinea+=1}
              rangeP.startIndex=result.endIndex
              return nextToken()
            case "salto":
               nlinea+=1
               rangeP.startIndex=result.endIndex
               return Token(expr.nombre,"","\\n",nlinea)
               //return nextToken

            case "otro":
              let sub = fileContent.substringWithRange(rangeP.startIndex..<result.startIndex)
                  rangeP.startIndex=result.startIndex
                  print("Token desconocido "+sub)
                  ManError.newError("Token desconocido: \(sub)",nlinea)
                  return nextToken()
            case "opUnit","oprMat","oprComp","oprLog":
              let sub = fileContent.substringWithRange(rangeP.startIndex..<result.endIndex)
              rangeP.startIndex=result.endIndex
              return Token(expr.nombre,sub,sub,nlinea)
            case "identificador":
              
              for expr2 in Expr.pres{//es palabra reservada?

                if let result = fileContent.regex(expr2.regex,range:rangeP.startIndex..<result.endIndex){

                  let sub = fileContent.substringWithRange(rangeP.startIndex..<result.endIndex)
                  rangeP.startIndex=result.endIndex
                  return Token(expr2.nombre,"",sub,nlinea)
                }
              }
              //no lo es
              let sub = fileContent.substringWithRange(rangeP.startIndex..<result.endIndex)
              rangeP.startIndex=result.endIndex
              let num = tablaSim.addId(sub)
              return Token(expr.nombre,String(num),sub,nlinea)
            case "numero":
              let sub = fileContent.substringWithRange(rangeP.startIndex..<result.endIndex.advancedBy(-1))
              rangeP.startIndex=result.endIndex.advancedBy(-1)
              return Token(expr.nombre,"",sub,nlinea)
            default:
              let sub = fileContent.substringWithRange(rangeP.startIndex..<result.endIndex)
              rangeP.startIndex=result.endIndex
              return Token(expr.nombre,"",sub,nlinea)
          }

        }
      }
      return nil
    }
    return nil


  }

}
