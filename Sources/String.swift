import Foundation
extension String {
  func regex(exp:String,range:Range<String.Index>) -> Range<String.Index>? {
  let regex = try! NSRegularExpression(pattern: exp,
      options: [])
  if let result = regex.firstMatchInString(self, options: [],
      range: self.NSRangeFromRange(range)){
      return self.rangeFromNSRange(result.range)
  }
  return nil
}
func regex(exp:String) -> Range<String.Index>? {
  let regex = try! NSRegularExpression(pattern: exp,
      options: [])
  if let result = regex.firstMatchInString(self, options: [],
      range: NSMakeRange(0, utf16.count)){
      return self.rangeFromNSRange(result.range)
  }
  return nil
}
func subString(ini:Int=0,end:Int=0) ->String{
  return self.substringWithRange(self.startIndex.advancedBy(ini)..<self.startIndex.advancedBy(end))
}
func rangeFromNSRange(nsRange : NSRange) -> Range<String.Index>? {
  //let utf16start = self.utf16.startIndex
  if let from = String.Index(self.utf16.startIndex + nsRange.location, within: self),
      let to = String.Index(self.utf16.startIndex + nsRange.location + nsRange.length, within: self) {
          return from ..< to
  }
  return nil
}
func NSRangeFromRange(range : Range<String.Index>) -> NSRange {
  let utf16view = self.utf16
  let from = String.UTF16View.Index(range.startIndex, within: utf16view)
  let to = String.UTF16View.Index(range.endIndex, within: utf16view)
  return NSMakeRange(utf16view.startIndex.distanceTo(from), from.distanceTo(to))
}

}
